{-# LANGUAGE OverloadedStrings #-}
module ScottyExample (run, isCorrectSequence, countLength) where

import Web.Scotty
import ScottyInternal

run :: Int -> IO ()
run port =
  scotty port $
  get "/v1/length" $ do
    sequence <- param "sequence"
    html $ createResponse $ countLength sequence