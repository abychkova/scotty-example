{-# LANGUAGE OverloadedStrings #-}
module ScottyInternal (
  createResponse,

  --for unit tests
  countLength,
  isCorrectSequence
) where

import Data.Monoid (mconcat)
import Data.Text.Lazy hiding (length)
import Text.Regex.Base.RegexLike (makeRegex, match, mrSubList)
import Text.Regex.PCRE.String (Regex)

createResponse :: Maybe Int -> Text
createResponse length =
  case length of
    Nothing -> "<h1>Wrong sequence!</h1>"
    Just value -> mconcat ["<h1>Sequence length is ", pack $ show value, "</h1>"]

countLength :: String -> Maybe Int
countLength sequence
  | isCorrectSequence sequence = Just (length sequence)
  | otherwise = Nothing

isCorrectSequence :: String -> Bool
isCorrectSequence = match regex
  where
    regex = makeRegex ("^[G,L,Y,S,E,Q,D,N,F,A,K,R,H,C,V,P,W,I,M,T]*$" :: String) :: Regex