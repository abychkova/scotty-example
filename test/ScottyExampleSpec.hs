module ScottyExampleSpec where

import Data.Text.Lazy hiding (length, map, replicate)
import ScottyInternal (isCorrectSequence, countLength)
import Test.Hspec

correctSequences = ["QQQQQQQQQQQQQQ", "QVKKLLSSEDWWWACDFF", "Q", "GLYSEQDNFAKRHCVPWIMT", ""]

incorrectSequences = ["X", "231", "hK", "glyseqdnfakrhcvpwit", "GLySEQDNFahKRHCVpWImT", "GLYSEQDNFAKRHCVPWIMT!"]

testRegexForSequence :: Spec
testRegexForSequence =
  describe "ScottyExample" $ do
    it "regex match correct sequence" $
      map isCorrectSequence correctSequences `shouldBe` replicate (length correctSequences) True
    it "regex dont match wrong sequence" $
      map isCorrectSequence incorrectSequences `shouldBe` replicate (length incorrectSequences) False

testCountLength :: Spec
testCountLength =
  describe "ScottyExample" $ do
    it "counts length of correct sequence" $
      map countLength correctSequences `shouldBe` map (Just . length) correctSequences
    it "nothing for incorrect sequence" $
      map countLength incorrectSequences `shouldBe` replicate (length incorrectSequences) Nothing