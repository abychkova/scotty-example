module Main where

import ScottyExampleSpec
import System.IO
import Test.Hspec
import ScottyExample (isCorrectSequence)

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  hspec $ do
    testRegexForSequence
    testCountLength