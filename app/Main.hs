module Main where

import ScottyExample
import System.Environment

main = do
  args <- getArgs
  case args of
    [] -> error "must supply a port number"
    [port] -> run $ read port